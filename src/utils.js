import axios from 'axios';

export const getContent = (url, username, password) => {
  const optionalCredentials = username && password ? `${username}:${password}@` : '';
  const URL = url.replace(/^(https?:\/\/)/, `$1${optionalCredentials}`)
  return axios.post(`${process.env.REACT_APP_API}/get-text`, {
    'url': URL
  }).then((res) => {
    return res.data;
  })
}