import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Container, Grid, CircularProgress } from "@material-ui/core";

import TranslationBlock from '../components/TranslationBlock';
import UrlCollector from '../components/UrlCollector';

const useStyles = makeStyles({
  loading: {
    textAlign: "center",
    padding: "2em",
  }
});

export default function Translate() {
  const classes = useStyles();
  const [content, setContent] = useState([]);
  const [capture, setCapture] = useState('');
  const [loading, setLoading] = useState(false);

  return (
    <Container maxWidth="xl">
      <Grid>
        <UrlCollector setLoading={setLoading} setContent={setContent} setCapture={setCapture} />

        {loading && <Container className={classes.loading}><CircularProgress /></Container>}

        {content.length > 0 && <TranslationBlock content={content} setContent={setContent} capture={capture} />}
      </Grid>
    </Container>
  );
}