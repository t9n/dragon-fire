import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';

import Translate from './translate/Translate';
import Login from './login/Login';
import Header from './components/Header';
import Error404 from './errors/Error404';
import ErrorBoundary from './errors/ErrorBoundary';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#666ad1',
      main: '#303f9f',
      dark: '#001970',
      contrastText: '#fff',
    },
    secondary: {
      light: '#63a4ff',
      main: '#1976d2',
      dark: '#004ba0',
      contrastText: '#ffffff',
    },
  },
});

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <ErrorBoundary>
          <Header />
          <Switch>
            <Route exact path="/">
              <Translate />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route path="*">
              <Error404 />
            </Route>
          </Switch>
        </ErrorBoundary>
      </Router>
    </ThemeProvider>
  );
}

