import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles({
  root: {
    textAlign: 'center',
  }
})

export default function Login() {
  const classes = useStyles();
  return (
    <Container maxWidth="lg" className={classes.root}>
      <h1>Welcome to Dragon Fire.</h1>
      <p>This is a placeholder for the login page.</p>
    </Container>
  )
}
