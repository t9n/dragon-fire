import React, { Component } from 'react';

import Error500 from './Error500';

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <Error500 />
    }
    return this.props.children;
  }
}