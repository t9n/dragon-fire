import React from 'react';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  center: {
    textAlign: 'center',
  },
  fourohfourContainer: {
    backgroundColor: theme.palette.primary.light,
    borderRadius: '50%',
    margin: '8% 0 2% 0',
    width: '400px',
    height: '400px',
    fontWeight: 600,
  },
  fourohfour: {
    textAlign: 'center',
    paddingTop: '140px',
    color: 'white',
    fontWeight: 600,
  },
  tagline: {
    fontWeight: 500,
    fontStyle: 'italic'
  },
  buttonLink: {
    textDecoration: 'none',
    color: 'inherit',
  },
  button: {
    marginTop: '2em'
  }
}));

export default function Error500() {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item className={classes.fourohfourContainer}>
        <Typography variant="h1" className={classes.fourohfour}>500</Typography>
      </Grid>
      <Grid item className={classes.center}>
        <Typography variant="h5" component="h2" className={classes.tagline}>Sorry, something has gone awry!</Typography>
        <Link to="/" className={classes.buttonLink}><Button className={classes.button} variant="outlined" color="primary">Go home</Button></Link>
      </Grid>
    </Grid>);
}