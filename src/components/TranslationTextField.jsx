import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';

export default function TranslationTextField(props) {
  const [translations, setTranslations] = useState([]);

  const handleTranslationChange = (event) => {
    let { id, value } = event.target;
    let index = id.split('-')[2];
    let newTranslationsArray = [...translations];
    newTranslationsArray.splice(index, 1, value);
    setTranslations(newTranslationsArray);
  }

  return (
    <TextField id={"welsh-translation-" + props.index} label="Translation" name={"welsh-translation-" + props.index} onChange={handleTranslationChange} variant="outlined" fullWidth />
  )
}
