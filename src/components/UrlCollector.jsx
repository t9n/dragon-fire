import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as utils from '../utils';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(1),
  },
  grid: {
    '& > *': {
      padding: theme.spacing(1),
    },
  },
}));

const UrlCollector = (props) => {
  const classes = useStyles();
  const [url, setUrl] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const [validationError, setValidationError] = useState(false);
  const [validationErrorText, setValidationErrorText] = useState('');
  const [credentialsRequired, setCredentialsRequired] = useState(false);
  const [usernameRequiredText, setUsernameRequiredText] = useState('');
  const [PasswordRequiredText, setPasswordRequiredText] = useState('');

  const handleUrlCollectorSubmit = (event) => {
    event.preventDefault();
    setValidationError(false);
    setCredentialsRequired(false);
    setValidationErrorText('');
    setUsernameRequiredText('');
    setPasswordRequiredText('');
    if (!/[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)?/.test(url)) {
      setValidationError(true);
      setValidationErrorText('Invalid URL')
    } else {
      props.setLoading(true)
      utils.getContent(url, username, password).then(res => {
        props.setLoading(false);
        props.setContent(res.content);
        props.setCapture(res.capture);
      })
        .catch(err => {
          if (err.response.status === 401) {
            props.setLoading(false);
            setCredentialsRequired(true);
            setUsernameRequiredText('Username required');
            setUsernameRequiredText('Password required');
          } else if (err.response.status === 500) {
            props.setLoading(false)
          }
        });
    }
  }

  return (
    <Container maxWidth="lg" className={classes.container}>
      <form noValidate autoComplete="off" onSubmit={handleUrlCollectorSubmit}>
        <Grid container className={classes.grid}>
          <Grid item xs={6}>
            <TextField id="url" label="URL" value={url} onChange={e => setUrl(e.target.value)} variant="outlined" fullWidth error={validationError} helperText={validationErrorText} />
          </Grid>
          <Grid item xs={2}>
            <TextField id="username" label="Username (optional)" name="username" value={username} onChange={e => setUsername(e.target.value)} variant="outlined" error={credentialsRequired} helperText={usernameRequiredText} />
          </Grid>
          <Grid item xs={2}>
            <TextField id="password" label="Password (optional)" value={password} onChange={e => setPassword(e.target.value)} variant="outlined" error={credentialsRequired} helperText={PasswordRequiredText} />
          </Grid>
          <Grid item xs={2}>
            <Button type="submit" variant="outlined">Get content</Button>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
}

export default UrlCollector;