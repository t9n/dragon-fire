import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  editContentItem: {
    marginBottom: theme.spacing(1),
  },
  buttonGrid: {
    textAlign: 'center',
    marginTop: theme.spacing(1)
  },
  form: {
    width: '100%',
  }
}));

export default function EditContentForm(props) {
  const classes = useStyles();

  const [editContentError, setEditContentError] = useState(false);
  const [editContentErrorText, setEditContentErrorText] = useState('');

  const saveEditedContentEntry = (event) => {
    event.preventDefault();
    let itemContent = document.getElementById("editContentItem").value;
    let contentCopy = [...props.content];
    // eslint-disable-next-line eqeqeq
    if (itemContent == contentCopy[props.editingItemIndex]) {
      cancelEditContentEntry()
    } else if (itemContent !== '') {
      contentCopy.splice(props.editingItemIndex, 1, itemContent)
      props.setContent(contentCopy);
      props.setEditingItemIndex(null);
      props.setEditContentItem('');
    } else {
      setEditContentError(true);
      setEditContentErrorText('Please enter valid content');
    }
  }

  const cancelEditContentEntry = () => {
    props.setEditingItemIndex(null);
    props.setEditContentItem('');
  }

  return (
    <form noValidate autoComplete="off" onSubmit={saveEditedContentEntry} className={classes.form}>
      <Grid container>
        <Grid item xs={8} className={classes.editContentItem}>
          <TextField id="editContentItem" label="Content" name={"edit-content-" + props.index} value={props.editContentItem} onChange={e => props.setEditContentItem(e.target.value)} variant="outlined" fullWidth error={editContentError} helperText={editContentErrorText} />
        </Grid>
        <Grid item xs={2} className={classes.buttonGrid}>
          <Button className={classes.editButtons} type="submit" variant="outlined" color="primary">Save</Button>
        </Grid>
        <Grid item xs={2} className={classes.buttonGrid}>
          <Button className={classes.editButtons} type="button" variant="outlined" color="secondary" onClick={cancelEditContentEntry}>Cancel</Button>
        </Grid>
      </Grid>
    </form>
  );
}