import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
  },
  bar: {
    backgroundColor: theme.palette.primary.main
  },
  title: {
    flexGrow: 1,
  },
  login: {
    flexShrink: 1,
  },
  link: {
    textDecoration: 'none',
    color: 'inherit',
  },
}));

const Header = () => {
  const classes = useStyles();
  return (
    <div style={{ width: '100%' }} className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.bar}>
          <div className={classes.title}><Link to="/" className={classes.link}><Typography variant="h6" className={classes.title}>
            <span role="img" aria-label="Dragon emoji">🐉</span> Dragon Fire
          </Typography></Link></div>
          <div className={classes.login}><Link to="/login" className={classes.link}><Button color="inherit">Login</Button></Link></div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;