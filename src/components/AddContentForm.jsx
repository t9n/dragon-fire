import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';


const useStyles = makeStyles((theme) => ({
  item: {
    margin: theme.spacing(1),
  },
}));

export default function AddContentItemForm(props) {
  const classes = useStyles();

  const [addContentFormVisible, setAddContentFormVisible] = useState(false);
  const [newContentItem, setNewContentItem] = useState('');
  const [newContentError, setNewContentError] = useState(false);
  const [newContentErrorText, setNewContentErrorText] = useState('');


  const revealAddContent = () => {
    addContentFormVisible ? setAddContentFormVisible(false) : setAddContentFormVisible(true);
  }

  const addContent = (event) => {
    event.preventDefault();
    if (newContentItem !== '') {
      let newContentList = [...props.content, newContentItem];
      props.setContent(newContentList);
      setNewContentItem('');
      setAddContentFormVisible(false);
      setNewContentError(false);
      setNewContentErrorText('');
    } else {
      setNewContentError(true);
      setNewContentErrorText('Please enter valid content');
    }
  }

  return (
    <Grid container direction="column">
      <Grid item className={classes.item}>
        <Button variant="contained" startIcon={addContentFormVisible ? null : <AddCircleOutlineIcon />} onClick={revealAddContent} color="primary">{addContentFormVisible ? 'Cancel' : 'Add content'}</Button>
      </Grid>
      <Grid item className={classes.item}>
        {
          addContentFormVisible &&
          <form noValidate autoComplete="off" onSubmit={addContent}>
            <Grid container direction="row" alignItems="center">

              <Grid item className={classes.item}>
                <TextField id="newContentItem" label="New content" variant="outlined" onChange={e => setNewContentItem(e.target.value)} placeholder="New content" error={newContentError} helperText={newContentErrorText} />
              </Grid>

              <Grid item className={classes.item}>
                <Button type="submit" variant="outlined" color="primary">Add</Button>
              </Grid>

            </Grid>
          </form>
        }
      </Grid>
    </Grid>);
}