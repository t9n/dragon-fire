import React from 'react';
import Grid from '@material-ui/core/Grid';
import AddContentForm from './AddContentForm';
import TranslationRow from './TranslationRow';
import CaptureContainer from './CaptureContainer';

const TranslationBlock = (props) => {
  return (
    <Grid container>
      <Grid item xs={6}>
        {props.content.map((text, index) =>
          <TranslationRow content={props.content} setContent={props.setContent} index={index} text={text} key={index} />
        )}
        <AddContentForm content={props.content} setContent={props.setContent} />
      </Grid>
      <Grid item xs={6}>
        <CaptureContainer capture={props.capture} display="flex" justifyContent="center" />
      </Grid>
    </Grid>
  );
}

export default TranslationBlock;