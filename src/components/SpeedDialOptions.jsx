import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme) => ({
  speedDial: {
    marginLeft: theme.spacing(1)
  }
}));

export default function EditContentForm(props) {
  const classes = useStyles();

  const originalDialsStatus = [];
  props.content.forEach(content => originalDialsStatus.push(false));
  const [dialsStatus, setOpenDial] = useState(originalDialsStatus);

  const handleClose = () => {
    let dials = [...dialsStatus];
    let allDialsClosed = dials.map(dial => false)
    setOpenDial(allDialsClosed);
  };

  const handleOpen = (event) => {
    let dials = [...dialsStatus];
    let index = event.target.parentElement.id
    dials.splice(index, 1, true)
    setOpenDial(dials);
  };

  const deleteContentEntry = (event) => {
    let name;
    if (event.target.parentElement.parentElement.parentElement.name) {
      name = event.target.parentElement.parentElement.parentElement.name
    } else if (event.target.parentElement.parentElement.name) {
      name = event.target.parentElement.parentElement.name;
    } else if (event.target.parentElement.name) {
      name = event.target.parentElement.name;
    } else {
      name = event.target.name;
    }
    let index = Number(name.split("_")[1])
    let contentCopy = [...props.content];
    contentCopy.splice(index, 1);
    props.setContent(contentCopy)
  }

  const actions = [
    { icon: <EditIcon />, name: 'Edit', click: props.editContentEntry },
    { icon: <DeleteIcon />, name: 'Delete', click: deleteContentEntry },
  ];

  return (
    <SpeedDial
      className={classes.speedDial}
      ariaLabel={props.text + " content element options dial"}
      icon={<SpeedDialIcon />}
      onClose={handleClose}
      onOpen={handleOpen}
      open={dialsStatus[props.index]}
      direction={'right'}
      name={'speedDial_' + props.index}
      id={props.index}
    >
      {actions.map((action) => (
        <SpeedDialAction
          key={action.name}
          icon={action.icon}
          tooltipTitle={action.name}
          onClick={action.click}
          name={action.name + '_' + props.index}
        />
      ))}
    </SpeedDial>
  );
}