import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  captureContainer: {
    maxWidth: '600px',
    margin: '24px auto'
  },
  capture: {
    maxWidth: '100%',
    height: 'auto',
  }
}));

export default function CaptureContainer(props) {
  const classes = useStyles();
  return (
    <div className={classes.captureContainer}>
      {props.capture && <img src={props.capture} alt="Screencapture of the fetched prototype" className={classes.capture} />}
    </div>
  )
}
