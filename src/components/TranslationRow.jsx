import React, { memo, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Grid from '@material-ui/core/Grid';

import EditContentForm from './EditContentForm';
import SpeedDialOptions from './SpeedDialOptions';
import TranslationTextField from './TranslationTextField';

const useStyles = makeStyles((theme) => ({
  gridItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    margin: theme.spacing(1),
  },
  item: {
    margin: theme.spacing(1),
  },
}));

export default memo(function TranslationRow(props) {
  const classes = useStyles();

  const [editingItemIndex, setEditingItemIndex] = useState(null);
  const [editContentItem, setEditContentItem] = useState('');

  const editContentEntry = (event) => {
    let name;
    let { target } = event;
    if (target.parentElement.parentElement.parentElement.name) {
      name = target.parentElement.parentElement.parentElement.name
    } else if (target.parentElement.parentElement.name) {
      name = target.parentElement.parentElement.name;
    } else if (target.parentElement.name) {
      name = target.parentElement.name;
    } else {
      name = target.name;
    }
    let index = Number(name.split("_")[1])
    let contentCopy = [...props.content];
    let itemToEdit = contentCopy[index];
    setEditingItemIndex(index);
    setEditContentItem(itemToEdit);
  }

  return (
    <Grid item xs={12} container className={classes.gridItem}>
      <Grid item xs={9} container>
        {props.index !== editingItemIndex ? <p className={classes.item}>{props.text}</p> :
          <EditContentForm index={props.index} editContentItem={editContentItem} setEditContentItem={setEditContentItem} editContentEntry={editContentEntry} editingItemIndex={editingItemIndex} setEditingItemIndex={setEditingItemIndex} content={props.content} setContent={props.setContent} />
        }
        <TranslationTextField index={props.index} item xs={10} />
      </Grid>
      <SpeedDialOptions item xs={1} text={props.text} index={props.index} editContentEntry={editContentEntry} content={props.content} setContent={props.setContent} />
    </Grid>
  )
})
